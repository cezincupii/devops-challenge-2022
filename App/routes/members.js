const router = require("express").Router();
const membersController = require("../controllers/members");
router.get("/members", membersController.getMemb);
router.post("/members", membersController.createMemb);

module.exports = router;
