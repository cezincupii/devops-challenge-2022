const sequelize = require("./db.js");
const Memb = sequelize.import("./members.js");
sequelize.sync().then(() => {
  console.log("Database and tables ok");
});

module.exports = {
  sequelize,
  Memb,
};
