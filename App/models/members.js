module.exports = (sequelize, DataTypes) => {
  return sequelize.define("members", {
    id: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      autoIncrement: true,
    },
    name: DataTypes.STRING,
    price: DataTypes.FLOAT,
  });
};
