const Memb = require("../models/index").Memb;

const getMemb = async (req, res) => {
  const membs = await Memb.findAll();
  if (membs.length > 0) {
    res.status(200).json({ message: "Members found", membs: membs });
  } else {
    res
      .status(404)
      .json({ message: "There are currently no members in the building" });
  }
};

const createMemb = async (req, res) => {
  const members = new Memb(req.body);
  if (members.name && members.price) {
    await members.save();
    res.status(201).send({
      message: "Member added!",
      members: members,
    });
  } else {
    res.status(500).json({
      message: "Invalid members payload",
    });
  }
};

module.exports = { getMemb, createMemb };
